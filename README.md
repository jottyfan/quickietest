# quickietest

Quickie for minetest - make the game for busy players a little easier in standard tasks


# Installation on Debian Linux

Having minetest installed (can be found in the debian repository), a folder `./minetest/mods` can be created if not yet exits. Go to that folder and clone this repository, using this command:

```bash
git clone git@gitlab.com:jottyfan/quickietest.git
```

Afterwards, the mod will be available to activate in the configuration of the game.
